var debug = require('debug')("resistor-calculator:picker");
var minimist = require('minimist');
var parseEng = require('parse-eng');

var Picker = {

	// @TODO: Move this into a util
	colours: [
		'Black',
		'Brown',
		'Red',
		'Orange',
		'Yellow',
		'Green',
		'Blue',
		'Violet',
		'Grey',
		'White'
	],

	// @TODO: Move this into a util
	tolerences: {
		'10': "Silver",
		'5': "Gold",
		'1': "Brown",
		'2': "Red",
		'0.5': "Green",
		'0.25': "Blue",
		'0.1': "Violet",
		'0.05': "Grey"
	},

	serieses: {
		6: [
			1.0, 1.5,
			2.2,
			3.3,
			4.7,
			6.8
		],
		12: [
			1.0, 1.2, 1.5, 1.8,
			2.2, 2.7,
			3.3, 3.9,
			4.7,
			5.6,
			6.8,
			8.2
		],
		24: [
			1.0, 1.1, 1.2, 1.3, 1.5, 1.6, 1.8,
			2.0, 2.2, 2.4, 2.7,
			3.0, 3.3, 3.6, 3.9,
			4.3, 4.7,
			5.1, 5.6,
			6.2, 6.8,
			7.5,
			8.2,
			9.1
		]
	},

	pickClosestValue: function (argv) {

		var args = minimist(argv);

		var series;
		var bands;
		var tolerance;
		var value;

		value = parseEng(args._[0]);

		if (args.s) {
			series = Picker.parseSeries(args.s);
		} else {
			series = 12;
		}

		if (args.b) {
			bands = Picker.parseBands(args.b);
		} else {
			bands = 4;
		}

		if (args.t) {
			tolerance = Picker.parseTolerance(args.t);
		} else {
			tolerance = 5;
		}

		value = Picker.getClosestValueForSeries(value, series);

		var stripes = Picker.convertToStripes(value, bands, tolerance);

		console.log("Closest value: " + value.toString() + ", Stripes: " + stripes.join(' '));

	},

	parseSeries: function (s) {

		s = s.toString();

		var result = s.replace(/^e?(6|12|24)$/i, '$1');

		if (!result) {
			console.warn("Invalid Series, defaulting to E12");
			return 12;
		} else {
			return parseInt(result);
		}

	},

	parseBands: function (b) {

		var result = parseInt(b);

		if (isNaN(result) || result > 5 || result < 4) {
			console.warn("Invalid number of bands, defaulting to 4");
			return 4;
		} else {
			return result;
		}

	},

	parseTolerance: function (t) {

		t = t.toString();

		var tolerences = "((" + Object.keys(Picker.tolerences).join(")|(") + "))";

		tolerences = tolerences.replace(/\./g, '\\.');

		var rxp = new RegExp("^" + tolerences + "%?$");

		var result = t.replace(rxp, '$1');

		if (!result) {
			console.warn("Invalid Series, defaulting to 5%");
			return 5;
		} else {
			return parseFloat(result);
		}

	},

	getClosestValueForSeries: function (v, s) {

		var power = Math.floor(Math.log10(v));

		v = v / Math.pow(10, power);

		var result = Picker.serieses[s][0];

		var current_diff = Math.abs(v - result);

		for (var i = 0; i < Picker.serieses[s].length; i++) {

			var new_diff = Math.abs(v - Picker.serieses[s][i]);

			if (new_diff < current_diff) {
				current_diff = new_diff;
				result = Picker.serieses[s][i];
			}

		}

		var last_diff = Math.abs(v - (Picker.serieses[s][0] * 10));

		if (last_diff < current_diff) {
			result = (Picker.serieses[s][0] * 10);
		}

		result = result * Math.pow(10, power);

		result = Math.round(result);

		return result;

	},

	convertToStripes: function (v, b, t) {

		var stripes = [];

		for(var i = 0; i < b - 2; i ++) {

			var digit = Picker.getDigitFromNumber(i, v);

			stripes.push(Picker.colours[digit]);

		}

		var power = Math.floor(Math.log10(v)) - (b - 3);

		debug(v, power);

		stripes.push(Picker.colours[power]);

		stripes.push(Picker.tolerences[t]);

		return stripes;

	},

	getDigitFromNumber: function (d, n) {

		var digit = String(n).charAt(d);

		return Number(digit);

	}

};

module.exports = Picker;
