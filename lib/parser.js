var inquirer = require("inquirer");
var math = require('mathjs');
var debug = require('debug')("resistor-calculator:parser");

var Parser = {

	// @TODO: Move this into a util
	colours: [
		'Black',
		'Brown',
		'Red',
		'Orange',
		'Yellow',
		'Green',
		'Blue',
		'Violet',
		'Grey',
		'White'
	],

	/**
	 * @TODO Add parsing of arguments
	 * @param {[]} args
	 */
	parseFromBands: function (args) {

		debug('#askForBands');

		debug(args);

		//if (args.length > 0) {

			//Parser.parseBandsFromArguments(Parser.parseBands);

		//} else {

			Parser.askForBands(Parser.parseBands);

		//}

	},

	/**
	 * @TODO Add tolerance parsing
	 * @TODO Add 'negative' multipliers
	 * @TODO Add error checking
	 * @TODO Add check to see if in E# series
	 * @param {[]} bands
	 */
	parseBands: function (bands) {

		debug('#askForBands');

		debug(bands);

		var num = bands.slice(0, bands.length - 2);
		var mul = bands[bands.length - 2];
		var tol = bands[bands.length - 1];

		debug(num, mul, tol);

		var multiplier = Parser.colours.indexOf(mul);

		var total = 0;

		for (var i = 0; i < num.length; i++) {

			total += Parser.colours.indexOf(num[i]) * math.pow(10, num.length - i - 1);

		}

		total = total * math.pow(10, multiplier);

		console.log(total);

	},

	/**
	 *
	 * @param {Function} callback
	 */
	askForBands: function (callback) {

		debug('#askForBands');

		var promptForBandColour = function (clb, previousAnswers) {

			if (!previousAnswers) previousAnswers = [];

			inquirer.prompt([
					{
						type: "list",
						name: "band",
						message: "What colour is the band?",
						default: "None",
						choices: [
							"None",
							"Black",
							"Brown",
							"Red",
							"Orange",
							"Yellow",
							"Green",
							"Blue",
							"Violet",
							"Grey",
							"White",
							"Gold",
							"Silver"
						]
					}
				],
				function (answers) {

					if (answers.band == 'None') {

						clb(previousAnswers);

					} else {

						previousAnswers.push(answers.band);

						promptForBandColour(clb, previousAnswers);

					}

				}
			);

		};

		promptForBandColour(callback);

	},

	parseBandsFromArguments: function (clb) {

		debug('#askForBands');

	}

};

module.exports = Parser;
