var debug = require('debug')("resistor-calculator:index");

module.exports = {

	/**
	 * Parse the raw command
	 * @TODO Add commands to calculate in series and parallel
	 * @param {[]} argv Arguments supplied
	 */
	parseCommand: function (argv) {

		debug(argv);

		switch(argv[0]){
			case "bands":
				var parser = require('./parser');
				parser.parseFromBands(argv.slice(1));
				break;
			case "pick":
				var picker = require('./picker');
				picker.pickClosestValue(argv.slice(1));
				break;
			default:
				console.error("Unknown command", argv);
				break;

		}

	},

};
